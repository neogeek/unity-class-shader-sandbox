﻿Shader "Scott/Displacement"
{
	Properties
	{
		_MainTex ("Texture", 2D) = "white" {}
	}
	SubShader
	{
		Tags { "RenderType"="Opaque" }
		LOD 100

		Pass
		{
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag

			#include "UnityCG.cginc"

			struct appdata
			{
				float4 vertex : POSITION;
				float2 uv : TEXCOORD0;
			};

			struct v2f
			{
				float2 uv : TEXCOORD0;
				UNITY_FOG_COORDS(1)
				float4 vertex : SV_POSITION;
			};

			sampler2D _MainTex;
			float4 _MainTex_ST;

			v2f vert (appdata v)
			{
				v2f o;
				// get the coords of the object in space and match it to screen pixels
				o.vertex = UnityObjectToClipPos(v.vertex);

				// convert coords to world coords then multiply by vertex data to
				// apply specific xyz because it return a vector3 by default
				float3 worldPos = mul(unity_ObjectToWorld, v.vertex).xyz;

				// move the verticies based on the xposition of the object on a sin wave
				o.vertex.y += sin(worldPos.z + _Time.w);

				o.uv = TRANSFORM_TEX(v.uv, _MainTex);
				return o;
			}

			fixed4 frag (v2f i) : SV_Target
			{
				fixed4 col = tex2D(_MainTex, i.uv);
				return col;
			}
			ENDCG
		}
	}
}
